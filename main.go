package main

import (
	database "enlabs-cron/config"
	"enlabs-cron/models"
	"fmt"
	"github.com/jasonlvhit/gocron"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/shopspring/decimal"
	"strconv"
)

func processTransactions() {
	// open database connection
	db, err := database.Open()
	if err != nil {
		fmt.Println("ERROR! " + err.Error())

		return
	}

	db.AutoMigrate(&models.Transaction{})

	// get last 10 transactions from database.
	var transactions []models.Transaction
	db.Raw("select * from (select t.*, row_number() over (order by id desc) as seqnum from transactions t ) t where mod(seqnum, 2) = 0 and deleted_at = null limit 10;").Scan(&transactions)

	fmt.Println("Transactions to be processed: " + strconv.Itoa(len(transactions)))

	for _, transaction := range transactions {

		fmt.Println("Processing transaction with Transaction ID: " + transaction.TransactionId)

		updateBalance(transaction.Amount, transaction.State, db)

		db.Delete(&transaction)
		fmt.Println("Done !")
	}

	database.Close()
}

func updateBalance(transactionAmount decimal.Decimal, transactionState string, db *gorm.DB) {
	// Update user balance
	var balance models.Balance
	db.First(&balance)
	switch transactionState {
	case "win":
		balance.Amount = balance.Amount.Sub(transactionAmount)
		// if balance amount is less then 0, then we are updating it to 0 because it can't be less than 0;
		if balance.Amount.LessThan(decimal.NewFromInt(0)) {
			balance.Amount = decimal.NewFromInt(0)
		}
	case "lost":
		balance.Amount = balance.Amount.Add(transactionAmount)
	default:
		return
	}

	db.Save(&balance)
}

func main() {
	godotenv.Load()

	gocron.Every(5).Minutes().Do(processTransactions)

	<-gocron.Start()

}
