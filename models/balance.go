package models

import (
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
)

type Balance struct {
	 gorm.Model
	 Amount decimal.Decimal `json:"amount" gorm:"type:numeric; default:0"`
}
