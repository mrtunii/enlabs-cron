# Cron Job For Processing Transactions

##### Task: "Every N minutes 10 latest odd records must be canceled and balance should be corrected. Canceled records shouldn't be processed twice."

## Used Packages

* [gocron](github.com/jasonlvhit/gocron) - For scheduling and running jobs.
* [gorm](github.com/jinzhu/gorm) - ORM for database interaction
* [godotenv](github.com/joho/godotenv) - For loading variables from .env file
* [decimal](github.com/shopspring/decimal) - For interaction with prices


## How to run

1. Clone repository
2. use `cp .env.example .env` command to copy env file and change database variables in it.
3. `go build main.go`
4. `go run main.go`

***By default cron job will run every 5 minutes***